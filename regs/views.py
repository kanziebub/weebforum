from django.shortcuts import render, redirect
from .forms import LoginForm, SignupForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm


# Create your views here.
def logIn(request):
    if request.method == "POST":
        form = LoginForm(data = request.POST)

        if form.is_valid():
            usernameInput = request.POST["username"]
            passwordInput = request.POST["password"]

            print(usernameInput, passwordInput)
            user = authenticate(request, username = usernameInput, password = passwordInput)

            if user is not None:
                login(request, user)
                return redirect('login')

        else:
            messages.error(request, 'Invalid entry')

    else:
        form = LoginForm()

    context = {
        "form" : form,
    }

    return render(request, "registration/login.html", context)

def logOut(request):
    logout(request)
    return redirect("login")

def signUp(request):
    form = SignupForm()
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    return render(request, 'registration/signup.html', {'form' : form})

# from django.contrib.auth import views
# from django.urls import path

# urlpatterns = [
#     path('login/', views.LoginView.as_view(), name='login'),
#     path('logout/', views.LogoutView.as_view(), name='logout'),

#     path('password_change/', views.PasswordChangeView.as_view(), name='password_change'),
#     path('password_change/done/', views.PasswordChangeDoneView.as_view(), name='password_change_done'),

#     path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
#     path('password_reset/done/', views.PasswordResetDoneView.as_view(), name='password_reset_done'),
#     path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
#     path('reset/done/', views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
# ]

