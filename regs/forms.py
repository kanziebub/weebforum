# from django import forms

# class LoginForm(forms.Form):
#     username = forms.CharField(widget=forms.TextInput(attrs={
#         'class' : 'form-control',
#         'placeholder' : 'Username',
#         'type' : 'text',
#         'required' : True
#     }))
#     password = forms.CharField(widget=forms.TextInput(attrs={
#         'class' : 'form-control',
#         'placeholder' : 'Password',
#         'type' : 'password',
#         'required' : True
#     }))

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm


class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'signIn-username',
        'placeholder' : 'Username',
        'type' : 'text',
        'style' : 'width: 20vw;',
        'required' : True
    }))

    password = forms.CharField(label="Password", widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'signIn-password',
        'placeholder' : 'Password',
        'type' : 'password',
        'style' : 'width: 20vw;',
        'required' : True
    }))

class SignupForm(UserCreationForm):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'signUp-username',
        'placeholder' : 'Username',
        'type' : 'text',
        'style' : 'width: 20vw;',
        'required' : True
    }))

    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class' : 'form-control',
        'id' : 'signUp-email',
        'placeholder' : 'Email',
        'type' : 'email',
        'style' : 'width: 20vw;',
        'required' : True
    }))

    password1 = forms.CharField(widget=forms.PasswordInput(attrs={
        'class' : 'form-control',
        'id' : 'signUp-password',
        'placeholder' : 'Password',
        'type' : 'password',
        'style' : 'width: 20vw;',
        'required' : True
    }), label='Password')
    
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={
        'class' : 'form-control',
        'id' : 'signUp-password',
        'placeholder' : 'Confirm Password',
        'type' : 'password',
        'style' : 'width: 20vw;',
        'required' : True
    }), label='Confirm Password')

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']