from django.contrib import admin
from django.urls import path, include
from . import views
from .views import logIn,logOut,signUp


app_name = 'regs'

# urlpatterns = [
#     path('accounts/', include('django.contrib.auth.urls')),
#     path('login/', views.index, name='index'),
# ]

urlpatterns = [
    path('login/', logIn, name='login'),
    path('logout/', logOut, name='logout'),
    path('signup/', signUp, name='signup'),
]
