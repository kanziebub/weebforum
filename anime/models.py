from django.db import models

# Create your models here.

class PostWibu(models.Model):
    title   = models.CharField(max_length=20)
    name    = models.CharField(max_length=20, default='Anonymous') # max_length = required
    content = models.TextField(max_length=10000)
    image   = models.ImageField(blank=True, null=True, upload_to='uploads')

    def __str__(self):
        return self.title