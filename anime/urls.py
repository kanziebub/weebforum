from django.contrib import admin
from django.urls import path
from . import views

app_name = 'anime'

urlpatterns = [
    path('anime-forum/', views.overview, name='overview' ),
    path('anime-forum/add-post', views.post_create, name='create'),
    path('anime-forum/<int:idx>/details', views.post_details, name='details'),
    path('anime-post/search', views.search, name='search'),
]
