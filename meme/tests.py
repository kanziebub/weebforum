from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from django.http import HttpRequest, request, response
from .models import MemePost
from . import forms
from . import views
from . import apps


    # ---------- Testing Apps ----------
class MemeForumTests (TestCase):
    def test_app_meme_forum(self):
        appname = apps.MemeConfig.name
        self.assertEquals(appname, 'meme')

    # ---------- Testing URLs ----------
class URLsForumTest(TestCase):
    def test_url_meme_forum_overview(self):
        response = self.client.get('/meme-forum/')
        self.assertEquals(response.status_code, 200)
        
    def test_url_meme_forum_add_post(self):
        response = self.client.get('/meme-forum/add-post')
        self.assertEquals(response.status_code, 200)

    def test_url_meme_forum_addpost(self):
        MemePost.objects.create(
            title='HELLO',
            name='WORLD',
            content='HELLO WORLD',
        )
        response = self.client.get('/meme-forum/1/details')
        self.assertEquals(response.status_code, 200)

    # ---------- Testing Models ----------
class ModelsTests (TestCase):
    def test_model_str(self):
        MemePost.objects.create(
            title='HELLO',
            name='WORLD',
            content='HELLO WORLD',
        )
        id_X = MemePost.objects.all()[0].id
        post = MemePost.objects.get(id=id_X)
        self.assertEquals(str(post), 'HELLO')


    def test_model_create_post_without_image_upload(self):
        MemePost.objects.create(
            title='HELLO',
            name='WORLD',
            content='HELLO WORLD',
        )
        num = MemePost.objects.all().count()
        self.assertEquals(num, 1)

    # def test_model_create_post_without_name(self):
    #     MemePost.objects.create(
    #         title='HELLO',
    #         content='HELLO WORLD',
    #     )
    #     num = MemePost.objects.filter(name='Anonymous').count()
    #     self.assertEquals(num, 1)
    

    # ---------- Testing Views ----------
class ViewsTests (TestCase):
    def test_view_forum_meme_overview_response(self):
        response = Client().get('/meme-forum/')
        self.assertEquals(response.status_code, 200)

    def test_view_forum_meme_create_post_response(self):
        response = Client().post(
            '/meme-forum/add-post', 
            data={
                'title': 'HELLO',
                'name': 'WORLD',
                'content': 'HELLO WORLD',
            })
        self.assertEquals(response.status_code, 302)

    def test_ajax_only(self):
        response = self.client.get('/meme-forum/meme_data?q=match')
        self.assertEqual(response.status_code, 200)

class loginTest(TestCase):
    def test_overview_not_logged_in(self):
        headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        response = Client().get('/meme-forum/', **headers)
        self.assertContains(response, 'login')

    def test_overview_logged_in(self):
        headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        response = Client().get('/meme-forum/', **headers)
        self.assertContains(response, 'add')

    def test_overview_not_logged_in(self):
        headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        response = Client().get('/meme-forum/add-post', **headers)
        self.assertContains(response, 'login')

    def test_overview_logged_in(self):
        headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        response = Client().get('/meme-forum/add-post', **headers)
        self.assertContains(response, 'title')
    





