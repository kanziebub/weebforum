from django.contrib import admin
from django.urls import path
from . import views

app_name = 'meme_forum'

urlpatterns = [
    path('meme-forum/', views.Post, name='Post' ),
    path('meme-forum/add-post', views.addPost, name='addPost'),
    path('meme-forum/<int:pk>/details', views.detailPost, name='detailPost'),
    path('meme-forum/meme_data', views.meme_data, name='search'),
]

