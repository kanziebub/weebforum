from django import forms
from .models import MemePost

# Create your forms here.

class MemeForm(forms.ModelForm):
    class Meta:
        model = MemePost
        fields = [
            'title',
            'name',
            'content',
            'image',
        ]
    title = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={
                'class' : 'form-control',
                'placeholder' : 'Enter post title',
                'type' : 'text',
            }))

    name = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={
                'class' : 'form-control',
                'placeholder' : 'Anonymous',
                'type' : 'text',
            }))

    content = forms.CharField(
        required=True,
        widget=forms.Textarea(
            attrs={
                'class' : 'form-control',
                'placeholder' : 'Write something...',
                'type' : 'text',
            }))

    image = forms.ImageField(
        required=False,
        widget=forms.FileInput())