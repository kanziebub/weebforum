from django.urls import path, include
from homepage.views import homepage, homepage_data

urlpatterns = [
    path('', homepage),
    path('homepage_data/', homepage_data)
]