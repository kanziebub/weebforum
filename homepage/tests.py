from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User
from .views import homepage, homepage_data
from .models import Email
from . import apps
# from django.utils.encoding import force_text
# import json

# Create your tests here.

class TestHome(TestCase):
    def test_app_homepage(self):
        appname = apps.HomepageConfig.name
        self.assertEquals(appname, 'homepage')

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_template_is_exist(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_func_is_used(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def test_model_is_created(self):
        Email.objects.create(subscribe_for_a_healthy_dose_of_waifu="example@abc.com")
        count = Email.objects.all().count()
        self.assertEqual(count, 1)

    def test_print_model(self):
        email = Email.objects.create(subscribe_for_a_healthy_dose_of_waifu="example@abc.com")
        self.assertEqual(str(email), email.subscribe_for_a_healthy_dose_of_waifu)

    def test_can_save_POST_request(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')

        response = c.post('/', data={"subscribe_for_a_healthy_dose_of_waifu" : "example@abc.com"})
        count = Email.objects.all().count()
        self.assertEqual(count, 1)

        self.assertEqual(response.status_code, 200)

        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('1', html_response)

    def test_url_data_is_exist(self):
        Email.objects.create(subscribe_for_a_healthy_dose_of_waifu='example@abc.com')
        response = self.client.post('/homepage_data/?q=example')
        html = response.content.decode('utf8')
        self.assertIn("example@abc.com", html)
        self.assertEqual(response.status_code, 200)

    def test_func_data_is_used(self):
        Email.objects.create(subscribe_for_a_healthy_dose_of_waifu='example@abc.com')
        found = resolve('/homepage_data/')
        self.assertEqual(found.func, homepage_data)