from django import forms
from .models import Email

class Email_Form(forms.ModelForm):
    class Meta:
        model = Email
        fields = ['subscribe_for_a_healthy_dose_of_waifu']
    
    error_messages = {
        'required' : 'Please Type'
    }
    subscribe_for_a_healthy_dose_of_waifu = forms.EmailField(widget=forms.TextInput(attrs={
        'placeholder' : 'imvvibu@ui.ac.id',
        'maxlength' : 50,
        'type' : 'email',
        'required' : True
    }))
