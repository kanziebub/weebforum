from django.shortcuts import render
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from homepage.forms import Email_Form
from homepage.models import Email

# Create your views here.

def homepage(request):
    form = Email_Form(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        form.save()
    email_count = Email.objects.all().count()
    response = {
        'email_form' : Email_Form,
        'email_count': email_count,
    }
    return render(request, 'home.html', response)

def homepage_data(request):
    ename = request.GET['q']
    data = Email.objects.filter(subscribe_for_a_healthy_dose_of_waifu__startswith=ename)
    elist = serializers.serialize('json', data)
    return HttpResponse(elist, content_type="text/json-comment-filtered")
