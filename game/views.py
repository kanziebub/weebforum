from django.shortcuts import render, redirect, resolve_url
from .models import GamingPost
from .forms import formGaming

from django.http import JsonResponse,HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

from django.core import serializers
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect



# Create your views here.
def forum(request):
    model = GamingPost.objects.all().order_by('-id')
    tally = model.count()
    response = {
        'posts' : model,
        'tally' : tally,
    }
    return render(request, 'forum_gaming_overview.html', response)

def tambahpost(request):

    form = formGaming()
    if (request.method == 'POST'):
        form = formGaming(request.POST, request.FILES)
        if (form.is_valid()):
            model = GamingPost()
            model.name = form.cleaned_data['name']
            model.title = form.cleaned_data['title']
            model.content = form.cleaned_data['content']
            model.image = form.cleaned_data['image']

            if (model.name == ''):
                model.name = 'Anonymous'

            model.save()
        return redirect('/gaming-forum/')
    context = {
        'form' : form,
    }
    return render(request, 'forum_gaming_addpost.html', context)

def postdetail(request, pk):

    model = GamingPost.objects.all().get(id=pk)
    response = {"model" : model}
    return render(request, "forum_gaming_details.html", response)

def title_data(request):
    stitle = request.GET['q']
    data = GamingPost.objects.filter(title__startswith=stitle)
    slist = serializers.serialize('json', data)
    return HttpResponse(slist, content_type="text/json-comment-filtered")