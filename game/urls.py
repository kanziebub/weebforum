from django.contrib import admin
from django.urls import path

from . import views

app_name = 'gaming_forum'

urlpatterns = [
    path('gaming-forum/', views.forum, name='forum' ),
    path('gaming-forum/add-post', views.tambahpost, name='tambahpost'),
    path('gaming-forum/<int:pk>/details', views.postdetail, name='postdetail'),
    path('gaming-forum/title_data/', views.title_data ),
]

