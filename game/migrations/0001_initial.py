# Generated by Django 3.1.4 on 2021-01-02 01:34

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GamingPost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default='Anonymous', max_length=20, null=True)),
                ('title', models.CharField(max_length=20)),
                ('content', models.TextField(max_length=10000)),
                ('image', models.ImageField(blank=True, null=True, upload_to='gaming')),
            ],
        ),
    ]
