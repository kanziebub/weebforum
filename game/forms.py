from django import forms
from .models import GamingPost

# Create your forms here.

class formGaming(forms.ModelForm):
    class Meta :
        model = GamingPost 
        fields = [
            'title',
            'name',
            'content',
            'image'
        ]
    title = forms.CharField(
        required = True,
        widget=forms.TextInput(
            attrs={
                'class' : 'form-control',
                'placeholder' : 'write your title here',
                'type' : 'text',
            }))

    name = forms.CharField(
        required = False,
        widget=forms.TextInput(
            attrs={
                'class' : 'form-control',
                'placeholder' : 'Default: Anonymous',
                'type' : 'text',
            }))

    content = forms.CharField(
        required= True,
        widget=forms.Textarea(
            attrs={
                'class' : 'form-control',
                'placeholder' : 'write your content here',
                'type' : 'text',
            }))

    image = forms.ImageField(
        required=False,
        widget=forms.FileInput())