from django.db import models

# Create your models here.

class GamingPost(models.Model) :
	name    = models.CharField(blank=True, null=True,max_length=20, default='Anonymous')
	title   = models.CharField(max_length=20)
	content = models.TextField(max_length=10000)
	image   = models.ImageField(blank=True, null=True, upload_to="gaming")

	def __str__(self):
		return self.title