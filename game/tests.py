from django.test import TestCase, Client
from django.http import HttpRequest, request, response
from django.urls import resolve, reverse
from .models import GamingPost
from .views import title_data
from . import views, apps, forms


# Create your tests here.
class GamingTest(TestCase):
	# Apps
	def test_app_forum_game(self):
		appname = apps.GameConfig.name
		self.assertEquals(appname, 'gaming_forum')

	# URLs
	def test_url_gaming_forum_ov(self):
		response = self.client.get('/gaming-forum/')
		self.assertEquals(response.status_code, 200)

	def test_url_gaming_forum_ap(self):
		response = self.client.get('/gaming-forum/add-post')
		self.assertEquals(response.status_code, 200)

	def test_url_objek_gaming_ap(self):
		GamingPost.objects.create(
			title='Tetris 2020',
			name='tetman',
			content='old but gold always'
		)
		response = self.client.get('/gaming-forum/1/details')
		self.assertEquals(response.status_code, 200)

	# Models
	def test_model_str(self):
		GamingPost.objects.create(
            title='Tetris 2020',
            name='tetman',
            content="old but gold always",
        )
		pk = GamingPost.objects.all()[0].id
		post = GamingPost.objects.get(id=pk)
		self.assertEquals(str(post), 'Tetris 2020')

	def test_model_post_non_image_valid(self):
		GamingPost.objects.create(title='Tetris 2020',name='tetman',content='old but gold always')
		check = GamingPost.objects.all().count()
		self.assertEquals(check, 1)

	# Views
	def test_view_ov(self):
		response = Client().get('/gaming-forum/')
		self.assertEquals(response.status_code, 200)

	def test_view_ap(self):
		response = Client().post(
			'/gaming-forum/add-post',
			data={
				'title': 'Tetris 2020',
				'name': 'tetman',
				'content': 'old but gold always'
			})
		self.assertEquals(response.status_code, 302)

	def test_view_ap_count(self):
		Client().post(
			'/gaming-forum/add-post',
			data ={
				'title': 'Tetris 2020',
				'name': 'tetman',
				'content': 'old but gold always'
 			})
		check = GamingPost.objects.filter(name='tetman').count()
		self.assertEquals(check, 1)

	def test_view_anon_count(self):
		Client().post(
			'/gaming-forum/add-post',
			data={
				'title': 'Tetris 2020',
				'content': 'old but gold always'
			})
		check = GamingPost.objects.filter(title='Tetris 2020').count()
		self.assertEquals(check, 1)

	def test_func_data_is_used(self):
		GamingPost.objects.create(title='genshin')
		found = resolve('/gaming-forum/title_data/')
		self.assertEqual(found.func, title_data)

	def test_url_data_is_exist(self):
		GamingPost.objects.create(title='genshin')
		response = self.client.post('/gaming-forum/title_data/?q=genshin')
		html = response.content.decode('utf8')
		self.assertIn("genshin", html)
		self.assertEqual(response.status_code, 200)