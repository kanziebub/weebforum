
$("#searchBox").keyup( function() {
    var keyword = $("#searchBox").val();

    if (keyword){
        $.ajax({
            url : "/anime-post/search?q=" + keyword,
            success : function(data) {
                var postsArray = data;
    
                $("#result").empty();  // clears existing data
                for (i = 0; i < 10; i++) {
                    // loads 10 objects' infos
                    idx = postsArray[i].pk;
                    title = postsArray[i].fields.title;
                    user = postsArray[i].fields.name;
    
                    $("#result").append(
                        '<tr><td class="postCard"><a href="'+idx+'/details">'
                        +'<p style="font-weight: 700;">'+title+'</p>'
                        +'<p>'+user+'</p></a></td></tr>'
                        +'<tr><td><div style="height: 20px;"></div></td></tr>'
                    );
                };
            }
            
        });
    }
});