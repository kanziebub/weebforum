[![pipeline](https://gitlab.com/kanziebub/weebforum/badges/master/pipeline.svg)](https://gitlab.com/kanziebub/weebforum/commits/master)
[![coverage](https://gitlab.com/kanziebub/weebforum/badges/master/coverage.svg)](https://gitlab.com/kanziebub/weebforum/commits/master)

# weebforum

- 1906293221 Nabila Khansa
- 1906285516 Zulfan Kasilas Dinca
- 1906293253 Natasya Zahra
- 1906293146 Marcia Nadin Pramisiwi
- 1906292950 Azka Fitria

Link herokuapp: d02-tk1.herokuapp.com

App: We(e)bForU(m)

App Description: With an ongoing global pandemic and everyone (supposedly) in quarantine, people are desperate for any form of entertainment and human contact. This app provides just that. A place where you can meet new people of similar entertainment interest(S).

Features:

    - Landing Page
        A welcome page that navigates to the available forums.
        1. Buttons that navigates to available forums
        2. A subscription form
        3. Subscribers as a model

    - Informative Page
        States enforced rules on each forums.
        1. Buttons that navigates to different section of the page
        2. A feedback-to-developers form
        3. User as a model with name and feedback as attributes

    - Meme Forum
        A place to post memes and be toxic within reason.
        1. Titles of posts are displayed and can be clicked to see each post's details
        2. A create post form
        3. Post as a model

    - Anime Forum
        A place to find cultured 3D beings.
        1. Titles of posts are displayed and can be clicked to see each post's details
        2. A create post form
        3. Post as a model

    - Gaming Forum
        A place to find gaming friends.
        1. Titles of posts are displayed and can be clicked to see each post's details
        2. A create post form
        3. Post as a model
