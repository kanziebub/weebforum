from django import forms
from .models import Feedback

class Feedback_Form(forms.ModelForm):
    feedback = forms.CharField(widget=forms.Textarea(attrs={'placeholder' : 'your thoughts about WebForU...', 'type' : 'text'}), label='')
    class Meta:
        model = Feedback
        fields = ('feedback',)