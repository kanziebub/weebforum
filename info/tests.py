from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import informative_page
from . import apps
from .models import Feedback
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

class InformativePageTest(TestCase):
    def test_app_info(self):
        appname = apps.InfoConfig.name
        self.assertEquals(appname, 'info')

    def test_url_informative_page_is_exist(self):
        response = Client().get('/informative-page')
        self.assertEqual(response.status_code, 200) 

    def test_using_informative_page_template(self):
        response = Client().get('/informative-page')
        html = response.content.decode('utf8')
        self.assertIn("FORUM RULES", html)
        self.assertIn("ABOUT US", html)
        self.assertIn("CONTACT US", html)
        self.assertTemplateUsed(response, 'informative_page.html')

    def test_using_informative_page_func(self):
        found = resolve('/informative-page')
        self.assertEqual(found.func, informative_page)
        
    def test_model_can_create_new_feedback(self):
        Feedback.objects.create(feedback='Mantap gan')
        counting_all_available_feedback = Feedback.objects.all().count()
        self.assertEqual(counting_all_available_feedback, 1)

    def test_feedback_saved_and_counted(self):
        user = User.objects.create(username='puyo')
        user.set_password('puyoganteng')
        user.save()
        c = Client()
        logIn = c.login(username='puyo', password='puyoganteng')
        response = c.post('/informative-page', data={'feedback': 'Mantaappp'})
        self.assertEqual(Feedback.objects.all().count(), 1)
        self.assertEqual(response.status_code, 200)
        new_response = self.client.get('/informative-page')
        html_response = new_response.content.decode('utf8')
        self.assertIn('1', html_response)
    
    def test_feedback_is_not_saved(self):
        response = Client().post('/informative-page', {'feedback': 'Mantaappp'})
        self.assertEqual(response.status_code, 302)

    def test_data(self):
        Feedback.objects.create(feedback='Mantap gan')
        response = self.client.post('/informative-page/data?q=Mantap')
        html = response.content.decode('utf8')
        self.assertIn("Mantap gan", html)
        self.assertEqual(response.status_code, 200)

    def test_get_returns_json_200(self):
        Feedback.objects.create(feedback="Mantap")
        response = self.client.get('/informative-page/data?q=Mantap')
        self.assertEqual(response['content-type'], "text/json-comment-filtered")
        self.assertEqual(response.status_code, 200)

    # def test_success_when_not_added_before(self):
    #     response = self.client.post('/add-item-to-collection')
    #     self.assertEqual(response.status_code, 200)
    #     self.assertJSONEqual(
    #         str(response.content, encoding='utf8'),
    #         {'status': 'success'}
    #     )