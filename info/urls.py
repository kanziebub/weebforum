from django.contrib import admin
from django.urls import path
from .views import informative_page, data

urlpatterns = [
    path('informative-page', informative_page, name='informative_page'),
    path('informative-page/data', data, name='data')
]