from django.shortcuts import render, redirect
from django.core import serializers
from django.db.models import Count
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib import messages
from .models import Feedback
from .forms import Feedback_Form

def informative_page(request):
    form = Feedback_Form(request.POST or None)
    feedback_count = Feedback.objects.all().count()
    response = {'feedback_form': Feedback_Form, 'feedback_count': feedback_count,}
    if (request.method =='POST'):
        if form.is_valid and request.user.is_authenticated:
            feedback = form.save(commit=False)
            feedback.save()
            messages.success(request, 'Feedback submitted successfully. Thank you for supporting us!')
            return render(request, 'informative_page.html', response)
        return redirect('login/')
    return render(request, 'informative_page.html', response) 

def data(request):
    feedbacks = Feedback.objects.filter(feedback__startswith = request.GET['q'])
    feedbacks_list = serializers.serialize('json', feedbacks)
    return HttpResponse(feedbacks_list, content_type="text/json-comment-filtered")